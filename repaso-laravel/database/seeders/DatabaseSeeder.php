<?php

namespace Database\Seeders;

use App\Models\Empresa;
use App\Models\Paquete;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table("transportistas")->delete();
        DB::table("paquetes")->delete();
        DB::table("empresas")->delete();

        Empresa::factory(20)->create();
        $this->call(TransportistaSeeder::class);
        Paquete::factory(40)->create();
    }
}
