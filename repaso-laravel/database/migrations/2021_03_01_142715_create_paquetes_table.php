<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaquetesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes', function (Blueprint $table) {
            // identificador, la dirección de entrega, un campo entregado, imagen
            $table->id();
            $table->string("direccion_entrega");
            $table->boolean("entregado")->default(false);
            $table->string("imagen");
            $table->foreignId("transportista_id");
            $table->timestamps();

            $table->foreign("transportista_id")->references("id")->on("transportistas")->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paquetes');
    }
}
