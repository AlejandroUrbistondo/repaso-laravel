<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTransportistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_transportista', function (Blueprint $table) {
            $table->foreignId("empresa_id");
            $table->unsignedBigInteger("transportista_id");

            $table->primary(["empresa_id", "transportista_id"]);
            $table->foreign("transportista_id")->references("id")->on("transportistas")->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign("empresa_id")->references("id")->on("empresas")->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_transportista');
    }
}
