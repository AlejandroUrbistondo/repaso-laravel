<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    use HasFactory;

    public $timestamp = false;
    public $table = "paquetes";

    public function transportistas()
    {
        return $this->belongsToMany(Transportista::class);
    }
}
