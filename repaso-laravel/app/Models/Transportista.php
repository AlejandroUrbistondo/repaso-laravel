<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transportista
 * @property string $nombre
 * @property string $apellidos
 * @property string $fechaPermisoConducir
 * @property string $imagen
 */
class Transportista extends Model
{
    use HasFactory;
    public $timestamp = false;
    public $table = "transportistas";

    public function empresas()
    {
        return $this->belongsToMany(Empresa::class);
    }

    public function paquetes()
    {
        return $this->hasMany(Paquete::class);
    }

    public function getAñosPermiso()
    {
        $fechaFormateada = Carbon::parse($this->fechaPermisoConducir);
        return $fechaFormateada->diffInYears(Carbon::now());
    }

    public function getRouteKeyName()
    {
        return "slug";
    }
}
