<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Illuminate\Http\Request;

class RestController extends Controller
{
    public function getTransportistas()
    {
        $transportistas = Transportista::all();
        return response()->json($transportistas);
    }    

    public function getTransportista(String $nombre)
    {            
        $busqueda = "%".$nombre."%";
        $transportista = Transportista::query()->where("nombre", "like", $busqueda)->get();

        return response()->json($transportista);
    }
}
