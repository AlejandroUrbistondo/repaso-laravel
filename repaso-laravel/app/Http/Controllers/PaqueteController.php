<?php

namespace App\Http\Controllers;

use App\Models\Paquete;
use App\Models\Transportista;
use Illuminate\Http\Request;

class PaqueteController extends Controller
{
    public function create()
    {
        $transportistas = Transportista::all();
        return view("paquetes.create", compact("transportistas"));
    }

    public function store(Request $request)
    {
        $paquete = new Paquete();
        $paquete->direccion_entrega = $request->direccion;
        $paquete->transportista_id = $request->transportista;
        if($request->imagen != null)
        {
            $nombre_imagen = $request->imagen->getClientOriginalName();
            $request->imagen->storeAs("img/paquetes", $nombre_imagen);
            $paquete->imagen = $nombre_imagen;
        }

        $paquete->save();

        return redirect()->route("transportistas.index");
    }
}
