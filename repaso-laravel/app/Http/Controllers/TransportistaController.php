<?php

namespace App\Http\Controllers;

use App\Models\Transportista;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TransportistaController extends Controller
{
    public function index()
    {
        $transportistas = Transportista::all();
        return view("transportistas.index", compact("transportistas"));
    }

    public function show(Transportista $transportista)
    {
        return view("transportistas.show", compact("transportista"));
    }

    public function entregar(Transportista $transportista)
    {
        $paquetes = $transportista->paquetes;
        foreach ($paquetes as $paquete) 
        {
            $paquete->entregado = true;
            $paquete->save();
        }
        return redirect()->route("transportistas.show", $transportista)->with("mensaje", "Paquetes entregados");
    }

    public function noEntregar(Transportista $transportista)
    {
        $paquetes = $transportista->paquetes;
        foreach ($paquetes as $paquete) 
        {
            $paquete->entregado = false;
            $paquete->save();
        }
        return redirect()->route("transportistas.show", $transportista)->with("mensaje", "Paquetes no entregados");
    }

    public function edit(Transportista $transportista)
    {
        return view("transportistas.edit", compact("transportista"));
    }

    public function update(Request $request, Transportista $transportista)
    {         
        $transportista->nombre = $request->nombre;
        $transportista->apellidos = $request->apellidos;
        $transportista->slug = Str::slug($transportista->nombre . "-". $transportista->apellidos);
        $transportista->fechaPermisoConducir = $request->fechaPermisoConducir;
        if($request->imagen != null)
        {
            $nombre_imagen = $request->imagen->getClientOriginalName();
            $request->imagen->storeAs("img/transportistas", $nombre_imagen);
            $transportista->imagen = $nombre_imagen;
        }
        $transportista->save();

        return redirect()->route("transportistas.show", compact("transportista"));
    }

    public function delete(Transportista $transportista)
    {
        $transportista->delete();
        return redirect()->route("transportistas.index");
    }
}
