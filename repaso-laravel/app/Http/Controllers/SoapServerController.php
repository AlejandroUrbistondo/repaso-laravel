<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapServer;

class SoapServerController extends Controller
{
    private $class = SoapServerFunciones::class;
    private $uri = "http://localhost/DWES 2º/repaso_laravel/repaso-laravel/public/api";
    private $url = "http://localhost/DWES 2º/repaso_laravel/repaso-laravel/public/api/wsdl";

    public function getServer()
    {
        $server = new SoapServer($this->url);
        $server->setClass($this->class);
        $server->handle();
        exit();
    }

    public function getWsdl()
    {
        $wsdl = new WebServicesWSDLDocument($this->class, $this->uri);
        $wsdl->formatOutput = true;
        header("Content-Type: text/xml");
        echo $wsdl->saveXML();
        exit();
    }
}
