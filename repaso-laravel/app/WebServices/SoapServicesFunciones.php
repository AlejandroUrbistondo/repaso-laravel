<?php


namespace App\WebServices;


use App\Models\Transportista;

class SoapServicesFunciones
{

    /**
     * @return App\Models\Transportista[]
     */
    public function getTransportistas()
    {
        return Transportista::all();
    }

    /**
     * @param string $nombre
     * @return App\Models\Transportista
     */
    public function getTransportista(string $nombre)
    {
        $busqueda = "%".$nombre."%";
        return Transportista::query()->where("nombre", "like", $busqueda)->get();
    }
}
