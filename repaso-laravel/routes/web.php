<?php

use App\Http\Controllers\PaqueteController;
use App\Http\Controllers\RestController;
use App\Http\Controllers\SoapServerController;
use App\Http\Controllers\TransportistaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
    return redirect()->route("transportistas.index");
})->name("home");

Route::get('transportistas', [TransportistaController::class, 'index'])->name('transportistas.index');

Route::get('transportistas/{transportista}', [TransportistaController::class, 'show'])->name('transportistas.show');

//Route::get('transportistas/{transportista}', [TransportistaController::class, 'show'])->name('transportistas.show')->middleware("auth");

Route::get('transportistas/{transportista}/entregar', [TransportistaController::class, 'entregar'])->name('transportistas.entregar');

Route::get('transportistas/{transportista}/noEntregar', [TransportistaController::class, 'noEntregar'])->name('transportistas.noEntregar');

Route::get('transportistas/{transportista}/editar', [TransportistaController::class, 'edit'])->name('transportistas.edit');

Route::put('transportistas/{transportista}/editar', [TransportistaController::class, 'update'])->name("transportistas.update");

Route::delete('transportistas/{transportista}/delete', [TransportistaController::class, 'delete'])->name('transportistas.delete');

Route::get('paquetes/crear', [PaqueteController::class, 'create'])->name('paquetes.crear');

Route::post('paquetes/crear/store', [PaqueteController::class, 'store'])->name("paquetes.store");

// SOAP

Route::any('api', [SoapServerController::class, "getServer"]);

Route::any('api/wsdl', [SoapServerController::class, "getWsdl"]);

// REST

Route::any('rest/transportistas', [RestController::class, "getTransportistas"]);

Route::any('rest/transportista', [RestController::class, "getTransportista"]);