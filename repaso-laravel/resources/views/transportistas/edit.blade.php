@extends('layouts.master') 
 
@section('titulo')
  Editar transportista
@endsection 
 

@section("contenido")
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Editar transportista</div>
                <div class="card-body"
                        style="padding:30px">
                    <form action="{{ route("transportistas.update", $transportista) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method("put")
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="nombre" id="nombre" class="form-control" required value="{{ $transportista->nombre }}">
                        </div>
                        <div class="form-group">
                            <label for="apellidos">Apellidos: </label>
                            <input type="text" name="apellidos" id="apellidos" class="form-control" required value="{{ $transportista->apellidos }}">
                        </div>
                        <div class="form-group">
                            <label for="fechaPermisoConducir">Fecha obtencion permiso conducir: </label>
                            <input type="date" class="form-control" name="fechaPermisoConducir" required value="{{ $transportista->fechaPermisoConducir }}">
                        </div>
                        <div class="form-group">
                            <label for="imagen">Imagen:</label>
                            <input type="file" class="form-control" name="imagen">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Editar transportista</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection