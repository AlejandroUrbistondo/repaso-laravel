@extends('layouts.master')
 
@section('titulo')
  Transportistas
@endsection 
 
@section('contenido')
  <div class="row">
    @foreach( $transportistas as $transportista )
      <div class="card m-1 bg-light border-secondary" style="width: 18rem;">
        <img class="card-img-top" src="{{asset("storage/img/transportistas/".$transportista->imagen)}}" alt="Imagen de {{$transportista->nombre}}">
        <div class="card-body">
          <a href="{{ route("transportistas.show", $transportista) }}"><h3 class="card-title">{{$transportista->nombre}}</h3></a>
          <h4>{{ count($transportista->paquetes->where("entregado", false)) }} paquetes pendientes de entrega</h4>
        </div>
      </div>
    @endforeach
  </div>
@endsection 