@extends('layouts.master') 
 
@section('titulo')
  Mostrar transportista
@endsection 
 
@section('contenido')
  @if (session("mensaje"))
      <h3 class="alert-warning">{{ session("mensaje") }}</h3>
  @endif
  <div class="row">
    <div class="col-sm-3">
      <img class="img-fluid" src="{{ asset("storage/img/transportistas/".$transportista->imagen) }}" alt="imagen de {{$transportista->nombre}}">
    </div>    
    <div class="col-sm-9">
      <h1>{{ $transportista->nombre . " " . $transportista->apellidos }}</h1>

      <h2>Años conduciendo: {{ $transportista->getAñosPermiso() }}</h2>

      <h2>Empresas</h2>
      <div class="row">
        <ul>
          @foreach($transportista->empresas as $empresa)
            <li>{{$empresa->nombre}}</li>
          @endforeach
        </ul>
      </div>

      <h2>Paquetes</h2>
      @if(count($transportista->paquetes) > 0)
        <div class="row">
          @foreach($transportista->paquetes as $paquete)
            <p>Paquete {{$paquete->id}} - {{$paquete->direccion_entrega}}:
              @if($paquete->entregado)
                entregado
              @else
                <strong>pendiente de entrega</strong>
              @endif
            </p>
          @endforeach
        </div>
      @endif

      <a class="btn btn-primary" href="{{ route("transportistas.entregar", $transportista) }}" role="button" id="entregar" name="entregar">Entregar todo</a>
      <a class="btn btn-outline-secondary" href="{{ route("transportistas.noEntregar", $transportista) }}" role="button">Marcar como no entregado</a>
      <a class="btn btn-secondary" href="{{ route("transportistas.edit", $transportista) }}" role="button">Editar</a>
      <form action="{{ route("transportistas.delete", $transportista) }}" method="post">
        @csrf
        @method("delete")
        <input type="submit" class="btn btn-secondary" value="Borrar">
      </form>
    </div>
  </div>
  <script>
    $(document).ready(function ()
    {
      $("entregar").click(function()
      {
        
      }),
    }),
  </script>
@endsection 