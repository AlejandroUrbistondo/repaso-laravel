@extends("layouts.master")

@section("titulo")
    Crear paquete
@endsection

@section("contenido")
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">Añadir un nuevo paquete</div>
                <div class="card-body"
                     style="padding:30px">
                    <form action="{{ route("paquetes.store") }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="direccion">Dirección de la entrega del paquete</label>
                            <input type="text" name="direccion" id="direccion" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="transportista">Transportistas</label>
                            <select name="transportista" id="transportista">
                                @foreach($transportistas as $transportista)
                                    <option value="{{$transportista->id}}">{{$transportista->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="imagen">Imagen:</label>
                            <input type="file" class="form-control" name="imagen">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
                                Añadir paquete
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
